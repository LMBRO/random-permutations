/*
 * main.cpp
 *
 *
 *
 *Three ways of generating a vector size N of random numbers from 1 to N
 *
 *1st: O(n²logn)
 *2nd: O(nlogn)
 *3rd: O(linear)
 * The analysis for 1 and 2 rely on the approximation of partial harmonic sums. I don't know if tighter bounds are possible.
 *  Created on: 2017-02-12
 *      Author: etudiant
 */
#include <cassert>
#include <stdexcept>
#include <random>
#include <vector>
#include <functional>
#include <iostream>
#include <ctime>
#include <numeric>
void profile(std::function<std::vector<unsigned int>(unsigned int)> const & f,
		std::vector<unsigned int> const & inputs) {

	for (unsigned int N : inputs) {
		std::clock_t c_start = std::clock();
		f(N);
		std::clock_t c_end = std::clock();
		std::cout << 1000.0 * (c_end - c_start) / CLOCKS_PER_SEC << "ms\t";
	}
}

int randint(unsigned int i, unsigned int j, std::mt19937 & rng) {
	if (i > j) {
		throw std::invalid_argument("Second argument must be greater than or equal to the fist.");
	}
	std::uniform_int_distribution<unsigned int> uni(i, j);
	return uni(rng);
}

std::vector<unsigned int> algo1(unsigned int N) {
	std::random_device rd;
	std::mt19937 rng(rd());
	std::vector<unsigned int> v (N);
	for (unsigned int i = 0; i < N; ++i) {
		bool ok = false;
		unsigned int icandidate;
		while (not ok) {
			icandidate = randint(1, N, rng);
			ok = true;
			for (unsigned int j = 0; j < i; j++) {
				if (v[j] == icandidate)
					ok = false;
			}
			if (ok)
				v[i] = icandidate;
		}

	}
	return v;
}
std::vector<unsigned int> algo2(unsigned int N) {
	std::random_device rd;
	std::mt19937 rng(rd());
	std::vector<unsigned int> v (N);
	std::vector<bool> seen (N+1);
	seen.resize(N+1);
	v.resize(N);
	for (unsigned int i = 0; i < N; ++i) {
		bool ok = false;
		unsigned int icandidate;
		while (not ok) {
			icandidate = randint(1, N, rng);
			ok = true;
			if (seen[icandidate] == true)
				ok = false;
			if (ok) {
				v[i] = icandidate;
				seen[icandidate] = true;
			}
		}

	}
	return v;
}
std::vector<unsigned int> algo3(unsigned int N) {
	std::random_device rd;
	std::mt19937 rng(rd());
	std::vector<unsigned int> v ( N );
	std::iota(std::begin(v), std::end(v), 1);
	for (unsigned int i = 1; i < N; ++i) {
		std::swap(v[i], v[randint(0, i, rng)]);
	}
	return v;
}
int main() {
//	auto v = algo3(5);
//	for (auto & e:v){
//		std::cout << e << "  ";
	std::function<std::vector<unsigned int>(unsigned int)> fn1 = algo1;
	std::function<std::vector<unsigned int>(unsigned int)> fn2 = algo2;
	std::function<std::vector<unsigned int>(unsigned int)> fn3 = algo3;
	profile(fn1, std::vector<unsigned int> { 250, 500, 1000, 2000 });
	std::cout << std::endl;
	profile(fn2, std::vector<unsigned int>{25000,50000,100000,200000,400000,800000});
	std::cout << std::endl;
	profile(fn3, std::vector<unsigned int>{100000,200000,400000,800000,1600000,3200000,6400000});
	return 0;
}

